<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementary">
		<?php 
		get_template_part( 'template-parts/teamsidebar-module', 'page' );
		get_template_part( 'template-parts/mediaboxsidebar-module', 'page' );
		//WP widegets
		dynamic_sidebar( 'sidebar-1' ); 
		//
		get_template_part( 'template-parts/usefullinkssidebar-module', 'page' );

		?>

	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
