<?php
/**
 * Template Name: Homepage
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );
			get_template_part( 'template-parts/headlinesnews-module', 'page' );
			get_template_part( 'template-parts/latestnews-module', 'page' );
			
			// If comments are open or we have at least one comment, load up the comment template.
	

			// End of the loop.
		endwhile;
		?>

	
				
		

		</main><!-- #main -->
		

	</div><!-- #primary -->
 
<?php get_sidebar(); ?>
<?php get_footer(); ?>