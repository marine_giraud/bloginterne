<div class="latestnews-container">
	<h2>Latest news</h2>
	<ul>
	 <?php
			// The Query
			$the_query2 = new WP_Query( array( 'cat' => -5 ) );
			 
			// The Loop
			if ( $the_query2->have_posts() ) {
			   
			    while ( $the_query2->have_posts() ) {
			        $the_query2->the_post();?>

			
				<li class="latestnews-container__item col-8-8">
				<div class="latestnews-container__img col-1-4">
				    <a href="<?php the_permalink() ?>"><?php the_post_thumbnail(array(100,100)); ?></a>
				</div>
				<div class="latestnews-container__textwrapper col-3-4">
				    <a class="latestnews-container__title" href="<?php the_permalink() ?>"><h2><?php the_title(); ?></h2></a>
				    <p><?php echo substr(strip_tags($post->post_content), 0, 100);?></p>
				    <!-- <?php the_content( 'Read the full post »' ); ?>-->
				</div>
				</li>
			
				<?php
			    }
			  
			} else { ?>
			    // no posts found
			 <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>  
			 <?php 
			}
			/* Restore original Post Data */
			wp_reset_postdata();
			?>
	</ul>
	
</div>
