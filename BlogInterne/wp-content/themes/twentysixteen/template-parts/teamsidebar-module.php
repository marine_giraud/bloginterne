<section class="team-container">
	<div class="team-container__img">
		<img src="http://placehold.it/250/0175bf/ffffff" alt="team portrait">
	</div>
	<div class="team-container__textwrapper">
		<mark>LIEN VERS "Vos interlocuteurs"</mark>
		<ul class="team-container__address">
			<li>Coralie Horgue</li>
			<li>Communication interne </li>
			<li><a href="tel:xX XX XX XX XX"></a>Tel: +33 (X)X XX XX XX XX</li>
			<li><a href="mailto:xxxx@euronews.com"></a>xxxx@euronews.com</li>
		</ul>
	</div>
</section>