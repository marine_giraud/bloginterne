<?php
/* Add your custom php code here */

define('PARENT_DIR', get_template_directory());
define('PARENT_URL', get_template_directory_uri());
define('CHILD_DIR', get_stylesheet_directory());
define('CHILD_URL', get_stylesheet_directory_uri());

// !!!! Style imported directly in wordpress default default style.css in child theme 
//enqueue font
	// wp_enqueue_style('Roboto Condensed', '//fonts.googleapis.com/css?family=Roboto+Condensed');
// Enqueue custom script
	wp_enqueue_script( 'twentysixteen-script', get_stylesheet_directory_uri() . '/assets/js/theme.js');

// unregister WP default plugin
// if (!function_exists('my_unregister_default_wp_widgets')) {
//     function my_unregister_default_wp_widgets() { 
//         unregister_widget('WP_Widget_Calendar');
//         unregister_widget('WP_Widget_Tag_Cloud');
//     }
//     // add_action('widgets_init', 'my_unregister_default_wp_widgets', 1);
//     require_once ('includes/widgets/widgets.php');
// }

require_once CHILD_DIR . '/includes/metabox/metabox.php';
//customising widget titles
//to add arrow icon
function g7_widgets_init() {
        register_sidebar(array(
            'name'          => __('Default Sidebar', 'g7theme'),
            'id'            => 'sidebar',
            'description'   => __('This is the main sidebar, located beside the main content.', 'g7theme'),
            'before_widget' => '<li id="%1$s" class="widget %2$s">',
            'after_widget'  => '</li>',
            'before_title'  => '<h2 class="block-title block-title widgettitle"><span>',
            'after_title'   => '<i class="fa fa-angle-right"></i></span></h2></a>',
            'before_title_link'   => '<a class="title_link" href="\n',
            

        ));
        $custom_sidebar = g7_option('sidebar');
        $i = 1;
        if (!empty($custom_sidebar)) {
            foreach ($custom_sidebar as $v) {
                if (trim($v) == '') {
                    continue;
                }
                register_sidebar(array(
                    'name'          => $v,
                    'id'            => g7_sidebar_id($v),
                    'description'   => __('This is a custom sidebar, located beside the main content.', 'g7theme'),
                    'before_widget' => '<li id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</li>',
                    'before_title'  => '<h2 class="block-title widgettitle"><span>',
                    'after_title'   => '<i class="fa fa-angle-right"></i></span></h2>'
                ));
                $i++;
            }
        }
        register_sidebar(array(
            'name'          => __('Footer 1', 'g7theme'),
            'id'            => 'footer1',
            'description'   => __('This widget area is located at the left side of footer area.', 'g7theme'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="block-title widgettitle"><span>',
            'after_title'   => '<i class="fa fa-angle-right"></i></span></h2>'
        ));
        register_sidebar(array(
            'name'          => __('Footer 2', 'g7theme'),
            'id'            => 'footer2',
            'description'   => __('This widget area is located at the center of footer area.', 'g7theme'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="block-title widgettitle"><span>',
            'after_title'   => '<i class="fa fa-angle-right"></i></span></h2>'
        ));
        register_sidebar(array(
            'name'          => __('Footer 3', 'g7theme'),
            'id'            => 'footer3',
            'description'   => __('This widget area is located at the right side of footer area.', 'g7theme'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="block-title widgettitle"><span>',
            'after_title'   => '<i class="fa fa-angle-right"></i></span></h2>'
        ));

        register_widget('G7_Posts_Widget');
        register_widget('G7_Flickr_Widget');
        register_widget('G7_Comments_Widget');
        register_widget('G7_Social_Widget');
        register_widget('G7_Ads125_Widget');
        register_widget('G7_Ads300_Widget');
        register_widget('G7_Video_Widget');
        register_widget('G7_Facebook_Widget');
        register_widget('G7_Reviews_Widget');
        register_widget('G7_Contact_Widget');
        register_widget('G7_Subpages_Widget');
    }
    add_action('widgets_init', 'g7_widgets_init');