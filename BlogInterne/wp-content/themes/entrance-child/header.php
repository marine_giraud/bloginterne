<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  // tracker methods like "setCustomDimension" should be called before "trackPageView"
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//piwik.euronews.lan/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->


</head>

<body <?php body_class(); ?>>


<div class="header-first-row header-desktop">
	<div class="topbar clearfix container">
		<div class="topbar__container row">
			<div class="topbar__languages-box topbar__item">
				<?php do_action('wpml_add_language_selector'); ?>
			</div>
			
			<div id="top" class="topbar__item">
				<?php echo g7_site_title(); ?>


			</div>
		
			<div class="topbar__search-form topbar__item">
				<?php include 'searchform.php'; ?>
			</div>
			
		</div>
		

		
		
	</div>
</div>
<div class="header-first-row header-mobile">
	<div class="topbar clearfix topbar__container">

		<div class="logo-mobile__img topbar__item">
			<div class="logo-tablet"><?php echo g7_site_title(); ?></div>
			<!-- <a href="/" class="logo-mobile"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-mobile.png" alt="Blog Next logo"></a> -->
		</div>
		<div class="topbar__search-form topbar__item">
			<?php include 'searchform.php'; ?>
			<div class="topbar__languages-box">
				<?php do_action('wpml_add_language_selector'); ?>
			</div>
		</div>		
	</div>
</div>
	
	<?php if(get_field('breaking_news_general')): ?>
	<input type="checkbox" id="toggle-1">
	<label class="toggle-button" for="toggle-1">X</label>
		<div class="slide-effect">
			<div class="clearfix container">
				
			
				<div class="breaking-news col-xs-12 row">
					<div class="col-xs-12">
						<h1><?php the_field('breaking_news_general'); ?></h1>
						
					</div>
				</div>	
			
			</div>
			
		</div>
	<?php endif ?>

	<div id="wrapper">

		<div class="container">

			


			<main>
				<?php g7_breadcrumb(); 
				
			// if (get_page()=="Actualités") {
			// 		dynamic_sidebar( 'header-widget' );
			// }	
			
?>