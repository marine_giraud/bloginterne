<form action="<?php echo esc_url(home_url('/')); ?>" class="search-form" method="get" role="search">
	<div class="search-form__container">
		<input type="text" class="search-form__input" name="s" placeholder="<?php _e('Search...', 'g7theme'); ?>">
		<button type="submit" class="search-form__button"><i class="fa fa-search"></i></button>
	</div>
</form>