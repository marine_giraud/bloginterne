<?php 
$image = get_field('widget_image'); 
?>
<section class="team-container">
	<a href="">Vos interlocuteurs</a>
	<div class="team-container__img">

		<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">

	</div>
	<div class="team-container__textwrapper">
	<?php the_field('widget_text'); ?>
	</div>
</section>