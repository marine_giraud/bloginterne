var gulp = require('gulp');
var sass = require('gulp-sass');
var jshint = require('gulp-jshint'); 
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
var imagemin = require('gulp-imagemin');
var plumber = require('gulp-plumber'); 
var notify = require('gulp-notify');
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var rename = require('gulp-rename');

 // Styles
 gulp.task('sass', function () {
 
    gulp.src('assets/styles/src/**/*.scss')

    .pipe(plumber(plumberErrorHandler))
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(concat('theme.css')) 
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('assets/styles/'))
    .pipe(autoprefixer())
    .pipe(livereload());
 
});

//scripts + uglify
gulp.task('js', function () {
 
 return gulp.src('assets/js/src/**/*.js')
 .pipe(plumber(plumberErrorHandler))
  .pipe(sourcemaps.init()) 
  .pipe(jshint()) 
  .pipe(jshint.reporter('fail')) 
  .pipe(concat('theme.js'))
  .pipe(sourcemaps.write())
  .pipe(uglify())
  .pipe(gulp.dest('assets/js'))
  .pipe(livereload());  

});

// images optimisation
gulp.task('img', function() {
 
  gulp.src('assets/images/src/*.{png,jpg,gif}')
 	.pipe(plumber(plumberErrorHandler))
 
    .pipe(imagemin({
 
      optimizationLevel: 7,
 
      progressive: true
 
    }))
 
    .pipe(gulp.dest('assets/images'))
    .pipe(livereload());
 
});

//watch task
gulp.task('watch', function() {
  
  livereload.listen();

  gulp.watch('assets/styles/src/**/*.scss', ['sass']);
 
  gulp.watch('assets/js/src/**/*.js', ['js']);
 
  gulp.watch('assets/images/src/*.{png,jpg,gif}', ['img']);
 
});

//Error handling
var plumberErrorHandler = { errorHandler: notify.onError({
 
    title: 'Gulp',
 
    message: 'Error: <%= error.message %>'
 
  })
 
};

gulp.task('dev', ['sass', 'js', 'img', 'watch']);
gulp.task('default', ['sass', 'js']);
