<?php 
/*
Plugin Name: Images and videos widget
Version: 1.0
Plugin URI: http://danielpataki.com
Description: Allows you to add an arbitrary featured item to the sidebar. Includes a title, image, description and a link.
Author: Daniel Pataki
Author URI: http://danielpataki.com/
*/


add_action( 'widgets_init', 'ip_init' );

function ip_init() {
	register_widget( 'ip_widget' );
}

class ip_widget extends WP_Widget
{

    public function __construct()
    {
        // Basic widget details
		// $widget_details = array(
		// 'classname' => 'ip_widget',
		// 'description' =>h 'Creates a featured item consisting of a title, image, description and link.'
		// );
    	add_action( 'admin_enqueue_scripts', array( $this, 'ip_assets' ) );
		parent::__construct( 'ip_widget', 'Images and videos widget', $widget_details );
    }

    public function widget( $args, $instance )
    {
        // Widget output in the front end
        echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) { ?>
			 <a class="widgettitle--link" href='<?php echo esc_url( $instance['link_url'] )?>'><?php echo $args['before_title'].apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];?></a>
			
		 <?php } ?>
			
			
			<div class="ip-image">
			<?php if($instance['image']){?>

				<a class="widgettitle--linkimage" href='<?php echo esc_url( $instance['link_url'] )?>'>
					<img src="<?php echo $instance['image'] ?>">
				</a>
			<?php } ?>
			<?php if($instance['video']){?>
					<video width="100%" controls> 
						<source src='<?php echo $instance['video'] ?>'>
					</video>
			<?php } ?>
			</div>


			<div class='ip-description'>
			<?php echo wpautop( esc_html( $instance['description'] ) ) ?>
			</div>

			<div class='ip-link'>
			<a href='<?php echo esc_url( $instance['link_url'] ) ?>'><?php echo esc_html( $instance['link_title'] ) ?></a>
			</div>
		<?php
		echo $args['after_widget'];
    }

    public function update( $new_instance, $old_instance ) {
        // Form saving logic - if needed
         return $new_instance;
    }

    public function form( $instance ) {
        // Backend Form
	  	$title = '';
	    if( !empty( $instance['title'] ) ) {
	        $title = $instance['title'];
	    }

	    $description = '';
	    if( !empty( $instance['description'] ) ) {
	        $description = $instance['description'];
	    }

	    $link_url = '';
	    if( !empty( $instance['link_url'] ) ) {
	        $link_url = $instance['link_url'];
	    }

	    
		$image = '';
		if(isset($instance['image'])){
			$image = $instance['image'];
		}

		$video = '';
		if(isset($instance['video'])){
			$video = $instance['video'];
		}

	    ?>
	    <p>
	        <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
	        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	    </p>

	    <p>
	        <label for="<?php echo $this->get_field_name( 'description' ); ?>"><?php _e( 'Description:' ); ?></label>
	        <textarea class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" type="text" ><?php echo esc_attr( $description ); ?></textarea>
	    </p>

	    <p>
	        <label for="<?php echo $this->get_field_name( 'link_url' ); ?>"><?php _e( 'Link URL:' ); ?></label>
	        <input class="widefat" id="<?php echo $this->get_field_id( 'link_url' ); ?>" name="<?php echo $this->get_field_name( 'link_url' ); ?>" type="text" value="<?php echo esc_attr( $link_url ); ?>" />
	    </p>

	  
	    		
		<p>
		     
		    <label for="<?php echo $this->get_field_name( 'image' ); ?>"><?php _e( 'Image:' ); ?></label>
		    <input name="<?php echo $this->get_field_name( 'image' ); ?>" id="<?php echo $this->get_field_id( 'image' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $image ); ?>" />
		    <input class="upload_image_button" type="button" value="Upload Image" />
		</p>
		
		<p>
		    <label for="<?php echo $this->get_field_name( 'video' ); ?>"><?php _e( 'Video:' ); ?></label>
		    <input name="<?php echo $this->get_field_name( 'video' ); ?>" id="<?php echo $this->get_field_id( 'video' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $video ); ?>" />
		    <input class="upload_image_button" type="button" value="Upload Video" />
		</p>
		<?php
    }

	public function ip_assets()
	{
	    wp_enqueue_script('media-upload');
	    wp_enqueue_script('thickbox');
	    wp_enqueue_script('ip-media-upload', plugin_dir_url(__FILE__) . 'ip-media-upload.js', array( 'jquery' )) ;
	    wp_enqueue_style('thickbox');
	}
}