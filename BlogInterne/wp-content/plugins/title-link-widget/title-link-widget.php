<?php 
/*
Plugin Name: title link widget
Description: Allows you to add a link to the widget title _ live actual widget title empty
*/


add_action( 'widgets_init', 'title_link_widget_init' );

function title_link_widget_init() {
	register_widget( 'title_link_widget' );
}

class title_link_widget extends WP_Widget
{

    public function __construct()
    {
        // Basic widget details
		// $widget_details = array(
		// 'classname' => 'title_link_widget',
		// 'description' =>h 'Creates a featured item consisting of a title, image, description and link.'
		// );
    	add_action( 'admin_enqueue_scripts', array( $this, 'ip_assets' ) );
		parent::__construct( 'title_link_widget', 'Title Link Widget', $widget_details );
    }

    public function widget( $args, $instance )
    {
        // Widget output in the front end
        echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) { ?>
			 <a class="widgettitle--link" href='<?php echo esc_url( $instance['link_url'] )?>'><?php echo $args['before_title'].apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];?></a>
			
		 <?php }

		echo $args['after_widget'];
    }

    public function update( $new_instance, $old_instance ) {
        // Form saving logic - if needed
         return $new_instance;
    }

    public function form( $instance ) {
        // Backend Form
	  	$title = '';
	    if( !empty( $instance['title'] ) ) {
	        $title = $instance['title'];
	    }

	    $description = '';
	    if( !empty( $instance['description'] ) ) {
	        $description = $instance['description'];
	    }

	    $link_url = '';
	    if( !empty( $instance['link_url'] ) ) {
	        $link_url = $instance['link_url'];
	    }

	    
		$image = '';
		if(isset($instance['image'])){
			$image = $instance['image'];
		}
		


	    ?>
	    <p>
	        <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
	        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	    </p>

	    

	    <p>
	        <label for="<?php echo $this->get_field_name( 'link_url' ); ?>"><?php _e( 'Link URL:' ); ?></label>
	        <input class="widefat" id="<?php echo $this->get_field_id( 'link_url' ); ?>" name="<?php echo $this->get_field_name( 'link_url' ); ?>" type="text" value="<?php echo esc_attr( $link_url ); ?>" />
	    </p>

	  
			    	
		<?php
    }

	public function ip_assets()
	{
	    wp_enqueue_script('media-upload');
	    wp_enqueue_script('thickbox');
	    wp_enqueue_script('ip-media-upload', plugin_dir_url(__FILE__) . 'ip-media-upload.js', array( 'jquery' )) ;
	    wp_enqueue_style('thickbox');
	}
}